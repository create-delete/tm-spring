package by.rmukhin.db.repository;

import by.rmukhin.db.entities.UserEntity;

import by.rmukhin.db.enums.Department;
import by.rmukhin.db.enums.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findByLogin(String login);

    List<UserEntity> findByDepartment(Department department);
    List<UserEntity> findByRole(Role role);
    List<UserEntity> findByDepartmentAndRole(Department department, Role role);
}
