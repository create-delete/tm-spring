package by.rmukhin.db.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "message")
@Scope(value = "prototype")
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_task")
    private TaskEntity idTask;

    @Column(name = "date_write", nullable = false)
    @Value("#{T(java.time.LocalDate).now()}")
    private LocalDate dateWrite;

    @Column
    private String title;

    @Column(nullable = false)
    @Value("Empty")
    private String text;

    @Column
    private String signature;

    public MessageEntity(TaskEntity idTask, LocalDate dateWrite, String title, String text, String signature) {
        this.idTask = idTask;
        this.dateWrite = dateWrite;
        this.title = title;
        this.text = text;
        this.signature = signature;
    }

}
