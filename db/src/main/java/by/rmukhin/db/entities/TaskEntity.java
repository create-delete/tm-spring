package by.rmukhin.db.entities;

import by.rmukhin.db.enums.Status;
import lombok.*;
import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "task")
@Scope(value = "prototype")
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String title;

    @Column
    private String description;

    @Column(name = "datestart")
    private LocalDate dateStart;

    @Column(name = "dateend")
    private LocalDate dateEnd;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_customer")
    private UserEntity customer;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "excutortask",
            joinColumns = @JoinColumn(name = "id_task"),
            inverseJoinColumns = @JoinColumn(name = "id_user"))
    private Set<UserEntity> executor = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "observertask",
            joinColumns = @JoinColumn(name = "id_task"),
            inverseJoinColumns = @JoinColumn(name = "id_user"))
    private Set<UserEntity> observer = new HashSet<>();

    @OneToMany(mappedBy = "idTask", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<MessageEntity> messages = new ArrayList<>();

    public TaskEntity(String title, String description, LocalDate dateStart, LocalDate dateEnd, Status status, UserEntity customer) {
        this.title = title;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.status = status;
        this.customer = customer;
    }

}
