package by.rmukhin.db.entities;

import by.rmukhin.db.enums.Department;
import by.rmukhin.db.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user")
@Scope(value = "prototype")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column
    private String signature;

    @Column
    @Enumerated(EnumType.STRING)
    private Department department;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "executor")
    private Set<TaskEntity> performTasks;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "observer")
    private Set<TaskEntity> observedTasks;

    @Column
    private boolean active;

    @Column
    private String fio;

    public UserEntity(String login, String password, String signature, Department department) {
        this.login = login;
        this.password = password;
        this.signature = signature;
        this.department = department;
    }

    public UserEntity(String login, String password, String signature, String fio, Department department, Role userRole, boolean active) {
        this.login = login;
        this.password = password;
        this.signature = signature;
        this.department = department;
        this.role = userRole;
        this.active = active;
        this.fio = fio;
    }
}
