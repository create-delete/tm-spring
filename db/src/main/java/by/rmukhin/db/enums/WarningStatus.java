package by.rmukhin.db.enums;

public enum WarningStatus {
    INFO,
    WARNING,
    ERROR
}
