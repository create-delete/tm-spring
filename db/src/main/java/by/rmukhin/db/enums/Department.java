package by.rmukhin.db.enums;

public enum Department {
    ADMINISTRATOR,
    DEVELOPER,
    QA,
    CTO,
    WITHOUTDEPARTMENT
}
