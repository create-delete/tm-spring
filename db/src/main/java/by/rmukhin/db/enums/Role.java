package by.rmukhin.db.enums;

import org.springframework.security.core.GrantedAuthority;

public enum Role {
    ADMINISTRATOR,
    LEAD,
    USER,
    TESTUSER;
}
