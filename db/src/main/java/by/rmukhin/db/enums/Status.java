package by.rmukhin.db.enums;

public enum Status {
    NEW,
    IN_WORK,
    ON_REVIEW,
    COMPLETE,
    TEST_STATUS
}
