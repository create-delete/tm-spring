package by.rmukhin.web.controllers;

import by.rmukhin.db.entities.UserEntity;
import by.rmukhin.service.impl.UserService;
import by.rmukhin.service.interfaces.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;
    @Autowired
    private UtilService utilServiceImpl;

    @GetMapping("/admin")
    public String adminPage(@AuthenticationPrincipal UserEntity user,
                            Model model) {
        model.addAttribute("users", userService.getAll());

        if(model.getAttribute("info") == null) {
            model.addAttribute("info", "Admin");
        }

        return "admin/adminMain";
    }

    // Get form with details current user by id
    @GetMapping("/admin/{id}")
    public String userDetails(@PathVariable long id, Model model) {
        model.addAttribute("user", userService.get(id));
        return "admin/userDetails";
    }

//    // Just get page add form
//    @GetMapping("/admin/add")
//    public String addForm(Model model) {
//        return "admin/addUser";
//    }
//
//    //    REGISTER NEW USER
//    @PostMapping("/admin/addUser")
//    public String addUser(
//            @RequestParam String login,
//            @RequestParam String password,
//            @RequestParam String reqPassword,
//            @RequestParam String signature,
//            @RequestParam String fio,
//            @RequestParam String department,
//            @RequestParam String userRole,
//            Model model
//    ) {
//        UserEntity user = userService.findByLogin(login);
//
//        //Check existing user
//        if (user != null) {
//            model.addAttribute("info", "User with this username already exists");
//        } else {
//            // Password and reqPassword is requared (cant be null)
//            if (!password.equals(reqPassword)) {
//                model.addAttribute("info", "Password and request password not equals");
//            } else {
//                //Create new user and save him
//                user = new UserEntity(login, password, signature, fio, Department.valueOf(department), Role.valueOf(userRole), true);
//                userService.add(user);
//                model.addAttribute("info", "User " + user.getLogin() + " was added successfully");
//            }
//        }
//
//        return adminPage(model);
//    }

    //   FILTER BY DEPARTMENT OR/AND ROLE
    @PostMapping("admin")
    public String filterByDepartmentOrRole(
            @RequestParam(required = false) String department,
            @RequestParam(required = false) String userRole,
            Model model) {

        List<UserEntity> usersForResult = new ArrayList<>();

        //filling in the list of users depending on the passed parameter
        if (userRole == "" && department == "") {
            usersForResult = userService.getAll();
        } else if (userRole == "") {
            usersForResult = userService.findByDepartment(department);
        } else if (department == "") {
            usersForResult = userService.findByRole(userRole);
        } else {
            usersForResult = userService.findByDepartmentAndRole(department, userRole);
        }

        model.addAttribute("users", usersForResult);

        return "admin/adminMain";
    }

}
