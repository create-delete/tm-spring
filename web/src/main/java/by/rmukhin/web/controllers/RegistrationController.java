package by.rmukhin.web.controllers;

import by.rmukhin.db.entities.UserEntity;
import by.rmukhin.db.enums.Department;
import by.rmukhin.db.enums.Role;
import by.rmukhin.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.HashSet;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("info", "Registration user");
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam String login,
            @RequestParam String password,
            @RequestParam String reqPassword,
            @RequestParam String signature,
            @RequestParam String fio,
            @RequestParam String department,
            @RequestParam String userRole,
            Model model
    ) {
        UserEntity user = userService.findByLogin(login);

        //Check existing user
        if (user != null) {
            model.addAttribute("info", "User with this username already exists");
        } else {
            // Password and reqPassword is requared (cant be null)
            if (!password.equals(reqPassword)) {
                model.addAttribute("info", "Password and request password not equals");
            } else {
                //Create new user and save him
                user = new UserEntity(login, password, signature, fio, Department.valueOf(department), Role.valueOf(userRole), true);
                userService.add(user);
            }
        }

        return "index";
    }
}
