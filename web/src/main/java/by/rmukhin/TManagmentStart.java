package by.rmukhin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TManagmentStart {
    public static void main(String[] args) {
        SpringApplication.run(TManagmentStart.class, args);
    }
}
