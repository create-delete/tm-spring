package by.rmukhin.service.interfaces;

import java.util.List;

public interface EntityService<T> {
    T add(T t);

    void delete(T t);

    List<T> getAll();

    T get(Long id);

    void update(T t);
}
