package by.rmukhin.service.interfaces;

import java.time.LocalDate;

public interface UtilService {
    String getHashString(String stringToHash);

    boolean checkPassword(String password, String stored);

    LocalDate getDateByString(String date);
}
