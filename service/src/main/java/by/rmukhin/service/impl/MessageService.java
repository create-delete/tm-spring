package by.rmukhin.service.impl;

import by.rmukhin.db.entities.MessageEntity;
import by.rmukhin.service.interfaces.EntityService;

import java.util.List;

public class MessageService implements EntityService<MessageEntity> {
    @Override
    public MessageEntity add(MessageEntity messageEntity) {
        return null;
    }

    @Override
    public void delete(MessageEntity messageEntity) {

    }

    @Override
    public List<MessageEntity> getAll() {
        return null;
    }

    @Override
    public MessageEntity get(Long id) {
        return null;
    }

    @Override
    public void update(MessageEntity messageEntity) {

    }
}
