package by.rmukhin.service.impl;

import by.rmukhin.db.entities.TaskEntity;
import by.rmukhin.service.interfaces.EntityService;

import java.util.List;

public class TaskService implements EntityService<TaskEntity> {
    @Override
    public TaskEntity add(TaskEntity taskEntity) {
        return null;
    }

    @Override
    public void delete(TaskEntity taskEntity) {

    }

    @Override
    public List<TaskEntity> getAll() {
        return null;
    }

    @Override
    public TaskEntity get(Long id) {
        return null;
    }

    @Override
    public void update(TaskEntity taskEntity) {

    }
}
