package by.rmukhin.service.impl;

import by.rmukhin.db.entities.UserEntity;
import by.rmukhin.db.enums.Department;
import by.rmukhin.db.enums.Role;
import by.rmukhin.db.repository.UserRepository;
import by.rmukhin.service.exceptions.GetUserException;
import by.rmukhin.service.interfaces.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements EntityService<UserEntity>{

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserEntity add(UserEntity user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(UserEntity user) {
        userRepository.delete(user);
    }

    @Override
    public List<UserEntity> getAll() {
        Iterable<UserEntity> allUsers = userRepository.findAll();
        return (List<UserEntity>) allUsers;
    }

    /**
     * find optional user.
     * If user not - throw Excepton extend RuntimeException
     *
     * @param id
     * @return user by id or GetUserException
     */
    @Override
    public UserEntity get(Long id) {
        Optional<UserEntity> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new GetUserException("Cant get user by id: " + id);
        }
    }

    @Override
    public void update(UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    public UserEntity findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public List<UserEntity> findByDepartment(String stringDepartment) {
        Department department = Department.valueOf(stringDepartment);
        return userRepository.findByDepartment(department);
    }

    public List<UserEntity> findByRole(String stringRole) {
        Role role = Role.valueOf(stringRole);
        return userRepository.findByRole(role);
    }

    public List<UserEntity> findByDepartmentAndRole(String stringDepartment, String stringRole) {
        Role role = Role.valueOf(stringRole);
        Department department = Department.valueOf(stringDepartment);
        return userRepository.findByDepartmentAndRole(department, role);
    }
}
