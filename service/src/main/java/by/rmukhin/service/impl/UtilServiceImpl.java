package by.rmukhin.service.impl;

import by.rmukhin.service.interfaces.UtilService;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

@Service
public class UtilServiceImpl implements UtilService {

    //need add exception handling
    // using for hash password
    @Override
    public String getHashString(String stringToHash) {
        MessageDigest digest = null;
        StringBuilder builder = new StringBuilder();

        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] strByte = digest.digest(stringToHash.getBytes());
        for (byte b : strByte) {
            builder.append(String.format("%02X ", b));
        }
        return builder.toString().trim();

    }

    @Override
    public boolean checkPassword(String password, String stored) {
        String compared;

        if (password != null || password.length() > 0) {
            compared = getHashString(password);
        } else {
            return false;
        }
        if (compared.equals(stored)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * get LocalDate by string
     *
     * @param date yyyy-mm-dd
     */
    @Override
    public LocalDate getDateByString(String date) {
        String[] splittedParam = null;
        LocalDate localDate = null;

        if (date != null || date.length() != 0) {
            splittedParam = date.split("-");
        }

        if (splittedParam[0].length() == 4 &&
            splittedParam[1].length() == 2 &&
            splittedParam[2].length() == 2) {
            localDate = LocalDate.of(
                    Integer.parseInt(splittedParam[0]),
                    Integer.parseInt(splittedParam[1]),
                    Integer.parseInt(splittedParam[2])
            );
        }
        return localDate;
    }
}
